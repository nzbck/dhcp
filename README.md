# DHCP

Support de code pour un TP DHCP

## Prérequis

- Install Terraform : 1.0.10
- Install Terragrunt : 0.36.12
- Add your ssh key and public ip address in : ```environements/aws/common.hcl```
- Configure your aws credential :
```
[dhcp]
aws_access_key_id = <key_id>
aws_secret_access_key = <access_key>
```

## Create aws environement

- Run ```terragrunt run-all apply``` in the following directories (in this order)

```agsl
environements/aws/dev/00_network
environements/aws/dev/10_instances
environements/aws/dev/15_network_interfaces
environements/aws/dev/20_security
```

## SSH to VM
- First ssh on the pio
```
$ ssh -A admin@<public_ip_pio>
```
- Then ssh on the VM
```
$ ssh 10.15.2.5
```