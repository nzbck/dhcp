provider "aws" {
  region  = var.region
  profile = var.profile
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.34.0"
    }
  }
  required_version = ">= 1.0"
}
