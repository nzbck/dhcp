variable "environment" {
  type        = string
  description = "Nom de l'environnement (ex : `ops`)"
}

variable "profile" {
  type        = string
  description = "Nom du profile aws à utiliser (ex : `dhcp`)"
}

variable "project_name" {
  type        = string
  description = "Nom du projet (ex : `dhcp`)"
}

variable "region" {
  type        = string
  description = "Région aws à utiliser (ex : `eu-west-1`)"
}

variable "ips_to_allow_for_pio" {
  type        = map(string)
  default     = {}
  description = <<EOF
  Map contenant les couples `"user_name" = "IP"` à autoriser
  dans le security_group de la CDS, du swift et du PIO
  (ex: `{"JeanPETIT" = "12.13.14.15/32", "UlysseTRENTETUN" = "16.17.18.19/32"}`)
  EOF
}
