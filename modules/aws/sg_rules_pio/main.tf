resource "aws_security_group_rule" "allow_ssh_from_ips" {
  for_each          = var.ips_to_allow_for_pio
  description       = "Allow ssh from ${each.key}"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = [each.value]
  security_group_id = data.aws_security_group.pio.id
}

resource "aws_security_group_rule" "allow_ssh_from_subnet2" {
  description       = "Allow ssh from subnet 2"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["10.15.2.0/24"]
  security_group_id = data.aws_security_group.pio.id
}