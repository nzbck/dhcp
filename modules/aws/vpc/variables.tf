variable "cidr_block" {
  type        = string
  description = "Cidr bloc à utiliser pour le vpc (ex : `10.0.0.0/16`)"
}

variable "environment" {
  type        = string
  description = "Nom de l'environnement (ex : `ops`)"
}

variable "profile" {
  type        = string
  description = "Nom du profile aws à utiliser (ex : `dhcp`)"
}

variable "project_name" {
  type        = string
  description = "Nom du projet (ex : `dhcp`)"
}

variable "region" {
  type        = string
  description = "Nom de la région aws où créer le vpc (ex : `eu-west-1`)"
}
