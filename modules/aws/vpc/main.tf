resource "aws_vpc" "main" {
  cidr_block           = var.cidr_block
  enable_dns_hostnames = true

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}"
  }

}

# PUBLIC ACCESS

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}"
  }
}

resource "aws_route_table" "public_subnets" {
  vpc_id = aws_vpc.main.id

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-public-subnets"
  }
}

resource "aws_route" "internet_access" {
  route_table_id         = aws_route_table.public_subnets.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_route_table" "private_subnets" {
  vpc_id = aws_vpc.main.id

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-private-subnets"
  }
}
