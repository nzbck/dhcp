resource "aws_instance" "main" {
  count                  = var.nb_instances
  ami                    = local.target_image_id
  instance_type          = var.instance_type
  subnet_id              = data.aws_subnet.main.id
  user_data              = templatefile("${path.module}/init.tpl", { ssh_keys_string = join(",", var.ssh_keys) })
  iam_instance_profile   = var.iam_instance_profile
  vpc_security_group_ids = [aws_security_group.main.id]
  private_ip             = cidrhost(data.aws_subnet.main.cidr_block, 5 + count.index)

  root_block_device {
    encrypted   = true
    volume_size = var.root_volume_size
    tags = {
      Name         = "${var.environment}-${var.name}-${count.index}-root"
      environment  = var.environment
      project_name = var.project_name
    }
  }

  credit_specification {
    cpu_credits = "standard"
  }

  tags = merge({
    Name            = "${var.environment}-${var.name}${count.index}"
    environment     = var.environment
    product         = var.product == null ? var.project_name : var.product
    project_name    = var.project_name
    type            = var.name
    custom_dns_name = "${var.name}${count.index}.node.${var.environment}.${var.project_name}.dhcp"
    can_be_shutdown = var.can_be_shutdown
  }, var.custom_instance_tags)

  metadata_options {
    http_tokens   = "required"
    http_endpoint = "enabled"
  }

  lifecycle {
    ignore_changes = [
      user_data,
      ami
    ]
  }
}

resource "aws_eip" "main" {
  count    = var.is_public ? var.nb_instances : 0
  instance = aws_instance.main[count.index].id
  vpc      = true
  tags = {
    Name         = "${var.environment}-${var.name}${count.index}"
    environment  = var.environment
    project_name = var.project_name
  }
}
