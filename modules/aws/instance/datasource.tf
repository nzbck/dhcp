data "aws_vpc" "main" {
  tags = {
    environment  = var.environment
    project_name = var.project_name
  }
}

data "aws_subnet" "main" {
  vpc_id = data.aws_vpc.main.id

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-${local.subnet_type}"
  }
}

data "aws_ami" "debian" {
  most_recent = true
  owners      = ["679593333241"] # Debian

  filter {
    name   = "name"
    values = ["debian-11-amd64-202*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

locals {
  subnet_type     = var.is_public ? "public" : var.private_subnet_name
  target_image_id = var.ami_id == null ? data.aws_ami.debian.image_id : var.ami_id
}
