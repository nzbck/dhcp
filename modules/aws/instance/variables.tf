variable "allow_direct_internet_access" {
  type        = bool
  default     = true
  description = <<EOF
    Boolean qui permet de régler l'egress rule de l'instance :
    <ul><li> `true` -> va autoriser les flux vers `0.0.0.0/0`</li>
    <li>`false` -> va autoriser les flux seulement vers le vpc</li></ul>
    Ça permet de vérouiller les flux pour vérifier que la configuration des proxy fonctionne bien sur les machines.
    Normalement, seul les machines avec des ip publiques et les proxys devraient
    être autorisés à accèder directement à internet.
  EOF
}

variable "ami_id" {
  type        = string
  default     = null
  description = "Id de l'ami à utiliser (ex : `ami-0d5aa862a2016cd3e`). Si non renseigné, la dernière version Debian 11 sera recherchée."
}

variable "can_be_shutdown" {
  type        = bool
  default     = false
  description = "Mettre à `true` si la machine peut être éteinte la nuit et le weekend (ex: true)"
}

variable "custom_instance_tags" {
  type        = map(string)
  default     = {}
  description = <<EOF
    Tags supplémentaires à ajouter lors de l'instanciation du module instance
    (ex : `{"equipe_=d" = "toto", "mon_tag" = "ma_valeur"}`)
  EOF
}

variable "environment" {
  type        = string
  description = "nom de l'environnement (ex: dev)"
}

variable "iam_instance_profile" {
  type        = string
  default     = ""
  description = "IAM instance profile à utiliser pour l'instance (ex : `ec2_ops`)"
}

variable "instance_type" {
  type        = string
  default     = "t3.nano"
  description = "Type d'instance à utiliser pour le bastion (ex: t3.nano)"
}

variable "is_public" {
  type        = bool
  default     = false
  description = "Mettre `true` pour mettre l'instance dans le subnet publique avec une ip publique"
}

variable "log_disk_size" {
  type        = number
  default     = 10
  description = "Taille du disque de /var/log à ajouter à l'instance (ex : `10`)"
}

variable "name" {
  type        = string
  description = "Nom de l'instance (ex: pio)"
}

variable "nb_instances" {
  type        = number
  default     = 1
  description = "Nombre d'instance à créer (ex : `2`)"
}

variable "private_subnet_name" {
  type        = string
  default     = null
  description = "Nom du subnet auquel et rattaché l'instance (ex : `private1`)"
}

variable "product" {
  type        = string
  default     = null
  description = "Nom du produit auquel et rattaché l'instance (ex : `dhcp`)"
}

variable "profile" {
  type        = string
  description = "Nom du profile aws à utiliser (ex : `dhcp`)"
}

variable "project_name" {
  type        = string
  description = "Nom du projet (ex: dhcp)"
}

variable "region" {
  type        = string
  description = "Nom de la région aws où créer le vpc (ex : `eu-west-1`)"
}

variable "root_volume_size" {
  type        = number
  default     = 8
  description = "Taille du root disque de l'instance (ex : `8`)"
}

variable "ssh_keys" {
  type        = list(string)
  default     = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCy2gcP4ONEUUJHKOlM8l1wzUZpvNw6D0LBU3zdQ2Ua6xzSGAciahP0Gk9nvyBWi9DxXGB1CXaKr5FHT6qOSJUXR6Gd57rVAvHQk1uJsTIno0nLy+5XQegTbIwFILyqjMKqV54Bi0cjmMbbMtIXOYCA2p4ph60C+b6CgvkG4Czo2e/wtqmuAmseVUUfHKy0dPhomMyhYrXZwlrprYzzQMcm/li2afWl3OZUt+3vVC+jaCS1xOXpohlBiXt8/ZM/M6AFZmBxAAOLcFZqeXEFavHAH0W2P4hTC6kK5bAvgIipp6jaS3j3N7B+3zXUaQJ5LMzgZTbvD4937ZFvhS8ro9EWbGlOM534Pf3QqLdOXq5l9MYlpt1unjxnTPMl90qfB9BrYMr/p4Di+iynfrayn8GNkSpPlH+FiW5QABo1/tyAne3bLkYtgCZANUmWfY13dkSktJQz4cMZxeFDbOK7Va3h0ZuD+hlSYCb3wrSHQgr3X2u4jQ+7KvKo/quzFW+B8HGStdw7/B7vnxzjM1yue+CtSwZQXl2eHDP0m+NOm3UxcU1D5rRZpfDIgUAiQP11U8FnXBgrtpCUi3OW0wg15foKznhAYfmJQkbQC+bqxnZCcZ/UGgi/LKLeMtUxq+nRFVuoBhBAI7PjNePJEiPV4UDNalNpkHfwngfv+I8JATdGRw== noel.zablocki@octo.com"]
  description = <<EOF
    Liste des clé ssh à installer sur la machine.
    (ex : `["ssh-rsa AUIEUAPAEUEeaieaiepap plop@octo.com","ssh-rsa AUIEAUIEUAIeauieauieue plip@octo.com"]`)
  EOF
}
