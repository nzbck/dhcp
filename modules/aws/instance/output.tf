output "main_public_ip" {
  value       = aws_eip.main.*.public_ip
  description = "Ip publique des instances"
}
