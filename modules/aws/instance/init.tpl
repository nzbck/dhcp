#!/bin/bash
%{ for ssh_key in split(",", ssh_keys_string) ~}
echo "${ssh_key}" >> /home/admin/.ssh/authorized_keys
%{ endfor ~}
