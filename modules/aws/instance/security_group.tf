resource "aws_security_group" "main" {
  name        = "${var.environment}-${var.name}"
  description = "Security group for ${var.name}"
  vpc_id      = data.aws_vpc.main.id

  tags = {
    Name         = "${var.environment}-${var.name}"
    environment  = var.environment
    project_name = var.project_name
  }
}

resource "aws_security_group_rule" "allow_egress_access" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = var.allow_direct_internet_access ? ["0.0.0.0/0"] : [data.aws_vpc.main.cidr_block]
  security_group_id = aws_security_group.main.id
  description       = "Allow egress"
}

resource "aws_security_group_rule" "allow_ping_from_everyone" {
  description       = "Allow icmp from common all"
  type              = "ingress"
  from_port         = 8
  to_port           = 0
  protocol          = "icmp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.main.id
}

#resource "aws_security_group_rule" "allow_ssh_from_all" {
#  description       = "Allow ssh from all"
#  type              = "ingress"
#  from_port         = 22
#  to_port           = 22
#  protocol          = "tcp"
#  cidr_blocks       = ["0.0.0.0/0"]
#  security_group_id = aws_security_group.main.id
#}

