resource "aws_security_group_rule" "allow_ssh_from_pio" {
  description              = "Allow ssh from pio"
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  source_security_group_id = data.aws_security_group.pio.id
  security_group_id        = data.aws_security_group.ec2-p2.id
}