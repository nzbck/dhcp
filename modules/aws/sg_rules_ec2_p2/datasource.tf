data "aws_security_group" "pio" {
  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.environment}-pio"
  }
}

data "aws_security_group" "ec2-p2" {
  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.environment}-ec2_p2"
  }
}