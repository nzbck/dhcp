variable "environment" {
  type        = string
  description = "Nom de l'environnement (ex : `dev`)"
}

variable "profile" {
  type        = string
  description = "Nom du profile aws à utiliser (ex : `dhcp`)"
}

variable "project_name" {
  type        = string
  description = "Nom du projet (ex : `dhcp-td`)"
}

variable "region" {
  type        = string
  description = "Région aws à utiliser (ex : `eu-west-1`)"
}
