resource "aws_network_interface" "main" {
  subnet_id       = data.aws_subnet.private1.id
  security_groups = [data.aws_security_group.main.id]

  attachment {
    instance     = data.aws_instance.main.id
    device_index = 1
  }
}

