data "aws_vpc" "main" {
  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}"
  }
}

data "aws_subnet" "private1" {
  vpc_id = data.aws_vpc.main.id

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-private1"
  }
}

data "aws_subnet" "private2" {
  vpc_id = data.aws_vpc.main.id

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-private2"
  }
}

data "aws_instance" "main" {
  instance_tags = {
    Name         = "${var.environment}-ec2_p20"
    environment  = var.environment
    project_name = var.project_name
  type = "ec2_p2" }
  filter {
    name   = "instance-state-name"
    values = ["running"]
  }

}

data "aws_security_group" "main" {
  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.environment}-ec2_p2"
  }
}
