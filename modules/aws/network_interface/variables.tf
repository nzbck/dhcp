variable "environment" {
  type        = string
  description = "Nom de l'environnement (ex : `dev`)"
}

variable "project_name" {
  type        = string
  description = "Nom du projet (ex : `dhcp`)"
}

variable "profile" {
  type        = string
  description = "Nom du profile aws à utiliser (ex : `dhcp`)"
}


variable "region" {
  type        = string
  description = "Nom de la région aws où créer le vpc (ex : `eu-west-1`)"
}
