data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_vpc" "main" {
  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}"
  }
}

data "aws_route_table" "public" {
  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-public-subnets"
  }
}

data "aws_route_table" "private" {
  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-private-subnets"
  }
}
