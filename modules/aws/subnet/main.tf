
resource "aws_subnet" "main" {
  cidr_block        = var.cidr_block
  vpc_id            = data.aws_vpc.main.id
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-${var.subnet_name_suffix}"
  }
}

resource "aws_route_table_association" "public_access" {
  count = var.enable_public ? 1 : 0

  route_table_id = data.aws_route_table.public.id
  subnet_id      = aws_subnet.main.id
}

resource "aws_route_table_association" "private_access" {
  count = var.enable_public ? 0 : 1

  route_table_id = data.aws_route_table.private.id
  subnet_id      = aws_subnet.main.id
}

