variable "cidr_block" {
  type        = string
  description = "Cidr block à utiliser pour la création du subnet (ex : `10.0.0.0/24`)"
}

variable "enable_public" {
  type        = bool
  default     = false
  description = <<EOF
    Mettre à `true` pour que ça soit un subnet public (avec ip publiques)
    ou à `false` pour que ça soit un subnet privé
  EOF
}

variable "environment" {
  type        = string
  description = "Nom de l'environnement (ex : `ops`)"
}

variable "profile" {
  type        = string
  description = "Nom du profile aws à utiliser (ex : `dhcp`)"
}

variable "project_name" {
  type        = string
  description = "Nom du projet (ex : `dhcp`)"
}

variable "region" {
  type        = string
  description = "Région aws à utiliser (ex : `eu-west-1`)"
}

variable "subnet_name_suffix" {
  type        = string
  description = <<EOF
    Suffix à utiliser pour le nom du subnet (ex : `public`).
    Le nom global sera
    `project_name-environment-subnet_name_suffix`
  EOF
}
