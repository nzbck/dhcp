data "aws_vpc" "main" {
  tags = {
    environment  = var.environment
    project_name = var.project_name
  }
}

data "aws_subnet" "public" {
  vpc_id = data.aws_vpc.main.id

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-public"
  }
}

data "aws_route_table" "private" {
  vpc_id = data.aws_vpc.main.id
  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-private-subnets"
  }
}
