variable "environment" {
  type        = string
  description = "Nom de l'environnement (ex : `ops`)"
}

variable "profile" {
  type        = string
  description = "Nom du profile aws à utiliser (ex : `dhcp`)"
}

variable "project_name" {
  type        = string
  description = "Nom du projet (ex : `dhcp`)"
}

variable "region" {
  type        = string
  description = "Région aws à utiliser (ex : `eu-west-1`)"
}
