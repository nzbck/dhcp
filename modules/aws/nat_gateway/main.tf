resource "aws_eip" "nat" {
  vpc = true

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}-nat-gateway"
  }
}

resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.nat.id
  subnet_id     = data.aws_subnet.public.id

  tags = {
    environment  = var.environment
    project_name = var.project_name
    Name         = "${var.project_name}-${var.environment}"
  }
}

resource "aws_route" "private_nat_gateway" {
  route_table_id         = data.aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.main.id
}
