terraform {
  source = "../../../../..//modules/aws/instance?"
}

locals {
  common_vars     = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

include {
  path = find_in_parent_folders()
}

inputs = {
  is_public       = true
  name            = "pio"
  nb_instances    = 1
}
