terraform {
  source = "../../../../..//modules/aws/instance?"
}

locals {
  common_vars     = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

include {
  path = find_in_parent_folders()
}

inputs = {
  private_subnet_name = "private2"
  name            = "ec2_p2"
  nb_instances    = 1
}
