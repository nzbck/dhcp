terraform {
  source = "../../../../..//modules/aws/vpc?"
}

locals {
  common_vars     = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

include {
  path = find_in_parent_folders()
}
