dependencies {
  paths = [
    "../01_subnet_public",
    "../02_subnet_private1"
  ]
}
terraform {
  source = "../../../../..//modules/aws/nat_gateway?"
}

locals {
  common_vars     = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

include {
  path = find_in_parent_folders()
}
