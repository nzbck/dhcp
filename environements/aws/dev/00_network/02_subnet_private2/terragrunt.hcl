dependencies {
  paths = ["../00_vpc"]
}
terraform {
  source = "../../../../..//modules/aws/subnet2?"
}

locals {
  common_vars     = read_terragrunt_config(find_in_parent_folders("common.hcl"))
}

include {
  path = find_in_parent_folders()
}

inputs = {
  cidr_block         = "10.15.2.0/24"
  subnet_name_suffix = "private2"
}
