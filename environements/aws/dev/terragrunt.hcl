locals {
  common_vars                    = read_terragrunt_config(find_in_parent_folders("common.hcl"))
  region                         = local.common_vars.locals.region
  profile                        = local.common_vars.locals.profile
  project_name                   = local.common_vars.locals.project_name
  ssh_keys                       = local.common_vars.locals.ssh_keys
  ips_to_allow_for_pio           = local.common_vars.locals.ips_to_allow_for_pio
  terraform_version_constraint   = local.common_vars.locals.terraform_version_constraint
}

inputs = {
  environment                    = "dev"
  cidr_block                     = "10.15.0.0/16"
  region                         = local.region
  profile                        = local.profile
  project_name                   = local.project_name
  ssh_keys                       = local.ssh_keys
  ips_to_allow_for_pio           = local.ips_to_allow_for_pio
}
