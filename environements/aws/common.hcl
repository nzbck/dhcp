locals {
  region          = "eu-west-1"
  profile         = "dhcp"
  project_name    = "dhcp-test"
  ssh_keys = [
    "ssh-rsa blabla== toto@octo.com"
  ]

  ips_to_allow_for_pio = {
    "NoelZablocki"       = "89.87.81.149/32"
  }
  terraform_version_constraint = "= 1.0.10"
}
