default: lint doc
doc: modules/*/*
	for file in $^ ; do \
		terraform-docs md $${file} > $${file}/README.md  ; \
	done

lint: lint-terraform-fmt lint-checkov lint-conftest

lint-terraform-fmt:
	terraform fmt -recursive modules

lint-checkov:
	checkov -d modules

lint-conftest:
	conftest -p $$CONFTEST_POLICIES_PATH test modules --ignore=".terraform-docs.yml"
